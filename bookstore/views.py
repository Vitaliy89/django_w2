from django.shortcuts import render
from django.http import HttpResponse

books = [
    {'id': 1,
     'title': 'Fluent Python',
     'released_year': 2015,
     'description': 'Python’s simplicity lets you become productive quickly,'
                    ' but this often means you aren’t using everything it has to offer.'
                    ' With this hands-on guide, you’ll learn how to write effective,'
                    ' idiomatic Python code by leveraging its best—and possibly most'
                    ' neglected—features. Author Luciano Ramalho takes you through '
                    'Python’s core language features and libraries,'
                    'and shows you how to make your code shorter, faster,'
                    'and more readable at the same time.',
     'author_id': 1},

    {'id': 2,
     'title': 'Task 3',
     'released_year': 2022,
     'description': 'создайте базовый юрл и функцию представление которая будет выводить'
                    ' список всех книг, а также в каждой книге должна быть ссылка'
                    ' на полное описание книги',
     'author_id': 2},

    {'id': 3,
     'title': 'Python',
     'released_year': 2017,
     'description': 'Python’s simplicity lets you become productive quickly,'
                    ' but this often means you aren’t using everything it has to offer.'
                    ' With this hands-on guide, you’ll learn how to write effective,'
                    ' idiomatic Python code by leveraging its best—and possibly most'
                    ' neglected—features.',
     'author_id': 1},
]

authors = [
    {'id': 1,
     'first_name': 'Luciano',
     'last_name': 'Ramalho',
     'age': 51 },

    {'id': 2,
     'first_name': 'Victor',
     'last_name': 'Kovtun',
     'age': 26 }
]

def base_list(reqeust, index):
    book = books[int(index) - 1]
    return render(reqeust, 'bookstore/index.html', context=book)

def base_books(reqeust):
    return render(reqeust, 'bookstore/base_books.html', context={'books': books})

def author_list(reqeust, index_author):
    author = authors[int(index_author) - 1]
    return render(reqeust, 'bookstore/author_index.html', context=author)
#
def author_book(reqeust, in_author):
    author_book = list(filter(lambda book: book['author_id'] == in_author, books))
    return render(reqeust, 'bookstore/author_book.html', context={'author_book': author_book})
